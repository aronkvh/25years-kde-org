---
title: KDE's 25th Anniversary
description: Celebrate with us 25 years of awesomeness
menu:
  main:
    weight: 1
parts:
  - title: Simple by default
    text: Plasma is designed to be easy to use.
    color: blue
    slides:
      - title: Launcher
        laptop: true
        src: /content/plasma-desktop/plasma-launcher.png
        text: The Plasma Launcher lets you quickly and easily launch applications, but it can do much more -- convenient tasks like bookmarking applications, searching for documents as you type, or navigating to common places help you getting straight to the point. With a history of recently started programs and opened files, you can return to where you left off. It even remembers previously entered search terms so you don't have to.
        alt: Screenshot Plasma with the launcher open
      - title: System Tray
        laptop: true
        src: /content/plasma-desktop/plasma-systemtray.png
        alt: Screenshot Plasma with the system tray open
        text: >
          Connect to your WiFi network; change the volume; switch to the next
          song or pause a video; access an external device; change the screen
          layout. All these and a lot more--directly from the system tray.

          To conserve the focus on what you're currently doing, any icon can be
          hidden if you like. Inactive icons hide themselves unless you tell
          them to stay where they are.
        class: bottom-right
      - title: Notifications
        src: /content/plasma-desktop/plasma-notifications.png
        alt: Screenshot Plasma with a notification
        laptop: true
        text: See active tasks and recent actions; read new e-mails; quickly reply to messages; see track changes or low battery notice; check for updates; interact with recently moved files or screenshots. Or enter "Do Not Disturb" mode to concentrate on your work.
        class: bottom-right
      - title: Discover
        src: /content/plasma-desktop/discover.png
        laptop: true
        text: Discover lets you manage the applications installed in your computer. Updating, removing or installing an application is only one click away. Supports [Flatpak](https://flatpak.org/), [Snaps](https://snapcraft.io/store) and the applications available in your Linux distribution.

---
