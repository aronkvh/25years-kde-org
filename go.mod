module 25years-kde-org

go 1.14

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss-v4 v1.0.0 // indirect
	github.com/thednp/bootstrap.native v0.0.0-20211008102921-0f9194c2200b // indirect
	github.com/twbs/bootstrap v4.6.0+incompatible // indirect
	invent.kde.org/websites/aether-sass v0.0.0-20210630080319-9a1cd0592db3 // indirect
)
